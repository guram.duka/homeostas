include(../../settings.pri)

*msvc {
    QMAKE_CFLAGS += -wd4996
}

*g++ {
    QMAKE_CFLAGS += -Wno-unused-but-set-variable -Wno-unused-parameter
}

TEMPLATE = lib
CONFIG += staticlib create_prl
INCLUDEPATH += include
HEADERS = \
    include/sqlite/sqlite3.h \
    include/sqlite/sqlite3ext.h
SOURCES = src/sqlite3.c
