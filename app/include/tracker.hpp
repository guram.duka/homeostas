/*-
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Guram Duka
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
//------------------------------------------------------------------------------
#ifndef TRACKER_HPP_INCLUDED
#define TRACKER_HPP_INCLUDED
//------------------------------------------------------------------------------
#pragma once
//------------------------------------------------------------------------------
#include "config.h"
//------------------------------------------------------------------------------
#include <memory>
#include <thread>
#include <future>
#include <mutex>
#include <atomic>
#include <condition_variable>
//------------------------------------------------------------------------------
#include "indexer.hpp"
#include "server.hpp"
#include "client.hpp"
//------------------------------------------------------------------------------
namespace homeostas {
//------------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////////
//------------------------------------------------------------------------------
class directory_tracker_interface {
public:
    virtual ~directory_tracker_interface() {}
    directory_tracker_interface() {}

    directory_tracker_interface(const directory_tracker_interface & o) {
        *this = o;
    }

    directory_tracker_interface(directory_tracker_interface && o) {
        *this = std::move(o);
    }

    directory_tracker_interface & operator = (const directory_tracker_interface & o) {
        key_ = o.key_;
        dir_user_defined_name_ = o.dir_user_defined_name_;
        dir_path_name_ = o.dir_path_name_;
        return *this;
    }

    directory_tracker_interface & operator = (directory_tracker_interface && o) {
        if( this != &o ) {
            key_ = std::move(o.key_);
            dir_user_defined_name_ = std::move(o.dir_user_defined_name_);
            dir_path_name_ = std::move(o.dir_path_name_);
        }
        return *this;
    }

    const auto & key() const {
        return key_;
    }

    auto & key(std::key512 && v) {
        key_ = std::move(v);
        return *this;
    }

    auto & key(const std::key512 & v) {
        key_ = v;
        return *this;
    }

    const auto & dir_user_defined_name() const {
        return dir_user_defined_name_;
    }

    auto & dir_user_defined_name(std::string && v) {
        dir_user_defined_name_ = std::move(v);
        return *this;
    }

    auto & dir_user_defined_name(const std::string & v) {
        dir_user_defined_name_ = v;
        return *this;
    }

    const auto & dir_path_name() const {
        return dir_path_name_;
    }

    auto & dir_path_name(std::string && v) {
        dir_path_name_ = std::move(v);
        return *this;
    }

    auto & dir_path_name(const std::string & v) {
        dir_path_name_ = v;
        return *this;
    }

    virtual void startup()   {}
    virtual void shutdown()  {}

    virtual bool is_local()  const { return true; }
    virtual bool is_remote() const { return false; }
    virtual std::key512 remote() const { return std::key512(std::leave_uninitialized); }
protected:
    std::key512 key_;
    std::string dir_user_defined_name_;
    std::string dir_path_name_;
private:
};
//------------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////////
//------------------------------------------------------------------------------
class directory_tracker : virtual public directory_tracker_interface {
public:
    virtual ~directory_tracker() {
        shutdown();
    }

    directory_tracker() {}

    const auto & oneshot() const {
        return oneshot_;
    }

    auto & oneshot(bool oneshot) {
        oneshot_ = oneshot;
        return *this;
    }

    static std::mutex trackers_mtx_;
    static std::vector<std::shared_ptr<directory_tracker>> trackers_;

    void startup();
    void shutdown();
protected:
    void connect_db();
    void detach_db();
    void worker();
    void make_path_name();

    std::string db_suffix_;
    std::string db_name_;
    std::string db_path_;
    std::string db_path_name_;

    std::unique_ptr<sqlite3pp::database> db_;

    std::shared_future<void> worker_result_;
    std::mutex mtx_;
    std::condition_variable cv_;

    bool started_  = false;
    bool shutdown_ = false;
    bool oneshot_  = false;
private:
    directory_tracker(const directory_tracker &) = delete;
    void operator = (const directory_tracker &) = delete;
};
//------------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////////
//------------------------------------------------------------------------------
class remote_directory_tracker_interface : virtual public directory_tracker_interface {
public:
    virtual ~remote_directory_tracker_interface() {}
    remote_directory_tracker_interface() {}

    remote_directory_tracker_interface(const remote_directory_tracker_interface & o) : directory_tracker_interface() {
        *this = o;
    }

    remote_directory_tracker_interface(remote_directory_tracker_interface && o) {
        *this = std::move(o);
    }

    remote_directory_tracker_interface & operator = (const remote_directory_tracker_interface & o) {
        directory_tracker_interface::operator = (o);
        server_public_key_ = o.server_public_key_;
        return *this;
    }

    remote_directory_tracker_interface & operator = (remote_directory_tracker_interface && o) {
        if( this != &o ) {
            directory_tracker_interface::operator = (o);
            server_public_key_ = std::move(o.server_public_key_);
        }
        return *this;
    }

    virtual bool is_local()  const { return false; }
    virtual bool is_remote() const { return true; }
    virtual std::key512 remote() const { return server_public_key_; }

    const auto & server_public_key() const {
        return server_public_key_;
    }

    auto & server_public_key(std::key512 && key) {
        server_public_key_ = std::move(key);
        return *this;
    }

    auto & server_public_key(const std::key512 & key) {
        server_public_key_ = key;
        return *this;
    }

    const auto & client_public_key() const {
        return client_public_key_;
    }

    auto & client_public_key(std::key512 && key) {
        client_public_key_ = std::move(key);
        return *this;
    }

    auto & client_public_key(const std::key512 & key) {
        client_public_key_ = key;
        return *this;
    }
protected:
    std::key512 server_public_key_;
    std::key512 client_public_key_;
private:
};
//------------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////////
//------------------------------------------------------------------------------
class remote_directory_tracker :
    virtual public remote_directory_tracker_interface,
    virtual public directory_tracker
{
public:
    virtual ~remote_directory_tracker() {
        shutdown();
    }

    remote_directory_tracker() {}

    virtual void startup();
    virtual void shutdown();

    virtual bool is_local()  const { return false; }
    virtual bool is_remote() const { return true; }
    virtual std::key512 remote() const { return remote_directory_tracker_interface::remote(); }

    static void server_module(socket_stream & ss, const std::key512 & peer_key);

    enum OperationCode {
        OperationCodeACK = 0,
        OperationCodeRST = 1,
        OperationCodeBAD = 2, // bad key
        OperationCodeRQB = 3, // request blocks
        OperationCodeRQC = 4  // request changes
    };

    struct entry_response {
        uint64_t parent_id;
        uint64_t entry_id;
        uint64_t mtime;
        uint32_t level;
        uint8_t is_dir;
        uint64_t file_size;
        uint64_t block_size;
        std::string name;
        std::key512 digest;

        size_t raw_bytes() const {
            return sizeof(entry_response) - sizeof(name)
                + (name.size() + 1) * sizeof(name[0])
                - (is_dir ? digest.ssize() + sizeof(file_size) + sizeof(block_size): 0);
        }

        int handle;
    };

    struct block_response {
        int64_t block_no;
        union {
            struct {
                uint8_t deleted : 1;
                uint8_t commit  : 1;
                uint8_t next    : 1;  // next entry
                uint8_t eot     : 1;  // end of transmission
            };
            uint8_t flags;
        };
        std::key512 digest;

        size_t raw_bytes() const {
            return sizeof(entry_response) - (block_no <= 0 ? digest.ssize() : 0);
        }
    };

    struct blocks_commit {
        std::map<uint64_t, std::vector<int64_t>> entries;

        auto & emplace_back(uint64_t entry_id, int64_t block_no) {
            auto e = entries.emplace(std::make_pair(entry_id, std::vector<int64_t>()));
            e.first->second.emplace_back(block_no);
            return *this;
        }
    };
protected:
    void worker();
    void server_worker(socket_stream & ss);

    void init_entries_cache() {
        if( st_sel_entry_ == nullptr )
            st_sel_entry_ = std::make_unique<sqlite3pp::query>(*db_, R"EOS(
                SELECT
                    parent_id,
                    name,
                    mtime,
                    is_dir
                FROM
                    entries
                WHERE
                    id = :id
            )EOS");
    }

    void drop_entries_cache() {
        for( auto & e : entries_cache_ ) {
            close(e.second.handle);
            e.second.handle = -1;
        }

        entries_cache_.clear();
        st_sel_entry_ = nullptr;
    }

    auto entry_cache_emplace(uint64_t entry_id, entry_response & e) {
        auto i = entries_cache_.emplace(entry_id, std::move(e));

        if( !i.second )
            throw std::xsystem_error(EFAULT, std::generic_category(), "undefined behavior", __FILE__, __LINE__);

        auto & sser     = i.first->second;
        sser.handle     = -1;

        return sser;
    }

    auto & entry_cache_emplace(uint64_t entry_id, sqlite3pp::query::iterator & e) {
        auto i = entries_cache_.emplace(
            e->get<uint64_t>("parent_id"), entry_response());

        if( !i.second )
            throw std::xsystem_error(EFAULT, std::generic_category(), "undefined behavior", __FILE__, __LINE__);

        auto & sser     = i.first->second;
        sser.handle     = -1;
        sser.name       = e->get<const char *>("name");
        sser.parent_id  = e->get<uint64_t>("parent_id");
        sser.entry_id   = entry_id;
        sser.mtime      = e->get<uint64_t>("mtime");
        sser.file_size  = e->get<uint64_t>("file_size");
        sser.block_size = e->get<uint64_t>("block_size");
        sser.level      = e->get<uint32_t>("level");
        sser.is_dir     = e->get<uint8_t>("is_dir");
        sser.digest     = e->get<std::key512>("digest");

        return sser;
    }

    auto entry_cache_path_name(uint64_t entry_id) {
        if( entry_id == 0 )
            return dir_path_name_;

        auto i = entries_cache_.find(entry_id);

        if( i == entries_cache_.end() ) {
            at_scope_exit( st_sel_entry_->reset() );
            st_sel_entry_->bind("id", entry_id);

            for( auto e = st_sel_entry_->begin(); e; e++ ) {
                auto j = entries_cache_.emplace(e->get<uint64_t>("parent_id"), entry_response());

                if( !j.second )
                    throw std::xsystem_error(EFAULT, std::generic_category(), "undefined behavior", __FILE__, __LINE__);

                auto & sser     = j.first->second;
                sser.handle     = -1;
                sser.name       = e->get<const char *>("name");
                sser.parent_id  = e->get<uint64_t>("parent_id");
                sser.entry_id   = entry_id;
                sser.mtime      = e->get<uint64_t>("mtime");
                sser.is_dir     = e->get<uint8_t>("is_dir");
            }

            i = entries_cache_.find(entry_id);
        }

        if( i == entries_cache_.end() )
            throw std::xsystem_error(EFAULT, std::generic_category(), "undefined behavior or database corrupted", __FILE__, __LINE__);

        return entry_cache_path_name(i->second.parent_id) + path_delimiter + i->second.name;
    }

    auto & entry_cache_file_handle(uint64_t entry_id, int mode) {
        auto i = entries_cache_.find(entry_id);

        if( i == entries_cache_.end() )
            throw std::xsystem_error(EFAULT, std::generic_category(), "undefined behavior or database corrupted", __FILE__, __LINE__);

        if( i->second.handle == -1 ) {
            auto path_name = entry_cache_path_name(entry_id);

            i->second.handle = open(path_name, mode);

            if( i->second.handle == -1 ) {
                auto err = errno;
                std::cerr << std::xsystem_error(err, std::system_category(), "fail open file: " + path_name, __FILE__, __LINE__) << std::endl;
            }
        }

        return i->second;
    }

    std::unordered_map<uint64_t, entry_response> entries_cache_;
    std::unique_ptr<sqlite3pp::query> st_sel_entry_;
    std::unique_ptr<client> channel_;
private:
};
//------------------------------------------------------------------------------
namespace tests {
//------------------------------------------------------------------------------
void tracker_test();
//------------------------------------------------------------------------------
} // namespace tests
//------------------------------------------------------------------------------
} // namespace homeostas
//------------------------------------------------------------------------------
namespace std {
//------------------------------------------------------------------------------
inline homeostas::socket_stream & operator << (
    homeostas::socket_stream & ss,
    const homeostas::remote_directory_tracker::entry_response & e)
{
#if BYTE_ORDER == LITTLE_ENDIAN
    ss << e.parent_id;
    ss << e.entry_id;
    ss << e.mtime;
    ss << e.level;
    ss << e.is_dir;
    ss << e.name;

    if( !e.is_dir ) {
        ss << e.file_size;
        ss << e.block_size;
        ss << e.digest;
    }
#elif BYTE_ORDER == BIG_ENDIAN
    ss << std::htole(e.parent_id);
    ss << std::htole(e.entry_id);
    ss << std::htole(e.mtime);
    ss << std::htole(e.level);
    ss << e.is_dir;
    ss << e.name;

    if( !e.is_dir ) {
        ss << std::htole(e.file_size);
        ss << std::htole(e.block_size);
        ss << e.digest;
    }
#endif

    return ss;
}
//------------------------------------------------------------------------------
inline homeostas::socket_stream & operator >> (
    homeostas::socket_stream & ss,
    homeostas::remote_directory_tracker::entry_response & e)
{
    ss >> e.parent_id;
    ss >> e.entry_id;
    ss >> e.mtime;
    ss >> e.level;
    ss >> e.is_dir;
    ss >> e.name;

    if( !e.is_dir ) {
        ss >> e.file_size;
        ss >> e.block_size;
        ss >> e.digest;
    }

#if BYTE_ORDER == BIG_ENDIAN
    e.parent_id  = std::letoh(e.parent_id);
    e.entry_id   = std::letoh(e.entry_id);
    e.mtime      = std::letoh(e.mtime);
    e.level      = std::letoh(e.level);
    e.file_size  = std::letoh(e.file_size);
    e.block_size = std::letoh(e.block_size);
#endif

    return ss;
}
//------------------------------------------------------------------------------
inline homeostas::socket_stream & operator << (
    homeostas::socket_stream & ss,
    const homeostas::remote_directory_tracker::block_response & e)
{
#if BYTE_ORDER == LITTLE_ENDIAN
    ss << e.block_no;
#elif BYTE_ORDER == BIG_ENDIAN
    ss << std::htole(e.block_no);
#endif
    ss << e.flags;

    if( e.block_no > 0 )
        ss << e.digest;

    return ss;
}
//------------------------------------------------------------------------------
inline homeostas::socket_stream & operator >> (
    homeostas::socket_stream & ss,
    homeostas::remote_directory_tracker::block_response & e)
{
    ss >> e.block_no;
#if BYTE_ORDER == BIG_ENDIAN
    e.block_no = std::letoh(e.block_no);
#endif
    ss >> e.flags;

    if( e.block_no > 0 )
        ss >> e.digest;

    return ss;
}
//------------------------------------------------------------------------------
inline homeostas::socket_stream & operator << (
    homeostas::socket_stream & ss,
    const homeostas::remote_directory_tracker::OperationCode & code)
{
    ss << uint8_t(code);
    return ss;
}
//------------------------------------------------------------------------------
inline homeostas::socket_stream & operator >> (
    homeostas::socket_stream & ss,
    homeostas::remote_directory_tracker::OperationCode & code)
{
    uint8_t b;
    ss >> b;
    code = decltype(code) (b);
    return ss;
}
//------------------------------------------------------------------------------
inline homeostas::socket_stream & operator << (
    homeostas::socket_stream & ss,
    const homeostas::remote_directory_tracker::blocks_commit & e)
{
    for( const auto & pair : e.entries ) {
#if BYTE_ORDER == LITTLE_ENDIAN
        ss << pair.first;
#elif BYTE_ORDER == BIG_ENDIAN
        ss << std::htole(pair.first);
#endif

        for( const auto & bno : pair.second )
#if BYTE_ORDER == LITTLE_ENDIAN
            ss << bno;
#elif BYTE_ORDER == BIG_ENDIAN
            ss << std::htole(bno);
#endif

        ss << int64_t(0);
    }

    ss << int64_t(0);

    return ss;
}
//------------------------------------------------------------------------------
inline homeostas::socket_stream & operator >> (
    homeostas::socket_stream & ss,
    homeostas::remote_directory_tracker::blocks_commit & e)
{
    for(;;) {
        uint64_t entry_id;
        ss >> entry_id;
#if BYTE_ORDER == BIG_ENDIAN
        entry_id = std::letoh(entry_id);
#endif

        if( entry_id == 0 )
            break;

        std::vector<int64_t> blocks;

        for(;;) {
            int64_t block_no;
            ss >> block_no;
#if BYTE_ORDER == BIG_ENDIAN
            block_no = std::letoh(block_no);
#endif

            if( block_no == 0 )
                break;

            blocks.emplace_back(block_no);
        }

        e.entries.emplace(entry_id, std::move(blocks));
    }
    return ss;
}
//------------------------------------------------------------------------------
} // namespace std
//------------------------------------------------------------------------------
#endif // TRACKER_HPP_INCLUDED
//------------------------------------------------------------------------------
