/*-
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Guram Duka
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
//------------------------------------------------------------------------------
#include "thread_pool.hpp"
#include "port.hpp"
#include "cdc512.hpp"
#include "tracker.hpp"
//------------------------------------------------------------------------------
namespace homeostas {
//------------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////////
//------------------------------------------------------------------------------
std::mutex directory_tracker::trackers_mtx_;
std::vector<std::shared_ptr<homeostas::directory_tracker>> directory_tracker::trackers_;
//------------------------------------------------------------------------------
void directory_tracker::connect_db()
{
    if( db_ == nullptr ) {
        db_ = std::make_unique<sqlite3pp::database>();

        if( access(db_path_, F_OK) != 0 && errno == ENOENT )
            mkdir(db_path_);

        db_->connect(
            db_path_name_,
            SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE
        );

        db_->execute_all(R"EOS(
            PRAGMA busy_timeout = 3000;
            PRAGMA page_size = 4096;
            PRAGMA journal_mode = WAL;
            PRAGMA count_changes = OFF;
            PRAGMA auto_vacuum = INCREMENTAL;
            PRAGMA cache_size = -2048;
            PRAGMA synchronous = NORMAL;
            /*PRAGMA temp_store = MEMORY;*/
        )EOS");
    }
}
//------------------------------------------------------------------------------
void directory_tracker::detach_db()
{
    db_ = nullptr;
}
//------------------------------------------------------------------------------
void directory_tracker::worker()
{
    at_scope_exit( detach_db() );

    directory_indexer di;
    di.modified_only(true);

    for(;;) {
        try {
            connect_db();
            di.reindex(*db_, dir_path_name_, &shutdown_);
        }
        catch( const std::exception & e ) {
            std::cerr << e << std::endl;
            detach_db();
        }

        std::unique_lock<std::mutex> lk(mtx_);

        if( cv_.wait_for(lk, std::chrono::seconds(60), [&] { return shutdown_ || oneshot_; }) )
            break;
    }
}
//------------------------------------------------------------------------------
void directory_tracker::make_path_name()
{
    auto remove_forbidden_characters = [] (const auto & s) {
        std::string t;

        for( const auto & c : s )
            if( (c >= 'A' && c <= 'Z')
                || (c >= 'a' && c <= 'z')
                || (c >= '0' && c <= '9')
                || c == '.'
                || c == '_'
                || c == '-' )
                t.push_back(c);

        return t;
    };

    auto make_digest_name = [] (const auto & s) {
        std::string t;

        cdc512 ctx(s.cbegin(), s.cend());

        if( std::slen(t.c_str()) > 13 )
            t = t.substr(0, 13);

        t.push_back('-');
        t += std::to_string(ctx);

        return t;
    };

    auto make_name = [&] (const auto & s) {
        std::string t = remove_forbidden_characters(s);

        if( t != s )
            t = make_digest_name(t);

        t = std::to_string36(key_.begin(), key_.end(), '\0', 0);

        return t;
    };

    db_path_ = home_path(false) + ".homeostas";
    db_name_ = make_name(dir_user_defined_name_.empty() ? dir_path_name_ : dir_user_defined_name_)
        + db_suffix_ + ".sqlite";
    db_path_name_ = db_path_ + path_delimiter + db_name_;
}
//------------------------------------------------------------------------------
void directory_tracker::startup()
{
    if( started_ )
        return;

    make_path_name();

    shutdown_ = false;
    worker_result_ = thread_pool_t::instance()->enqueue(&directory_tracker::worker, this);
    started_ = true;
}
//------------------------------------------------------------------------------
void directory_tracker::shutdown()
{
    if( !started_ )
        return;

    std::unique_lock<std::mutex> lk(mtx_);
    shutdown_ = true;
    lk.unlock();
    cv_.notify_one();

    worker_result_.wait();
    started_ = false;
}
//------------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////////
//------------------------------------------------------------------------------
void remote_directory_tracker::server_module(socket_stream & ss, const std::key512 & peer_key)
{
    remote_directory_tracker module;
    module.client_public_key_ = peer_key;
    module.server_worker(ss);
}
//------------------------------------------------------------------------------
void remote_directory_tracker::server_worker(socket_stream & ss)
{
    auto send_changes = [&] {
        // assemble server side changes packet and send to client
        // read and send and requested blocks
        connect_db();

        {
            sqlite3pp::command st_rt_ins(*db_, R"EOS(
                INSERT INTO remote_trackers (key, mtime) VALUES (:key, :mtime)
            )EOS");

            sqlite3pp::command st_rt_upd(*db_, R"EOS(
                UPDATE remote_trackers SET mtime = :mtime WHERE key = :key
            )EOS");

            sqlite3pp::transaction tx(*db_, true);

            st_rt_ins.bind("key", client_public_key_, sqlite3pp::nocopy);
            st_rt_ins.bind("mtime", clock_gettime_ns());
            db_->exceptions(false);
            st_rt_ins.execute();
            db_->exceptions(true);

            if( st_rt_ins.rc() == SQLITE_CONSTRAINT_UNIQUE ) {
                st_rt_upd.bind("key", client_public_key_, sqlite3pp::nocopy);
                st_rt_upd.bind("mtime", clock_gettime_ns());
                st_rt_upd.execute();
            }

            tx.commit();
        }

        sqlite3pp::query st_sel_parent(*db_, R"EOS(
            SELECT
                parent_id,
                name,
                mtime,
                is_dir
            FROM
                entries
            WHERE
                id = :id
        )EOS");

        auto st_rt_sel_text = [] (const char * tail) {
            return std::cat(R"EOS(
            SELECT
                e.parent_id,
                e.level,
                e.name,
                e.mtime,
                e.file_size,
                e.block_size,
                e.is_dir,
                e.digest,
                b.block_digest,
                a.entry_id,
                a.block_no,
                a.deleted
            FROM
                remote_tracking AS a
                    JOIN entries AS e
                    ON a.entry_id = e.id
                    LEFT JOIN blocks_digests AS b
                    ON a.entry_id = b.entry_id
                        AND a.block_no = b.block_no
            WHERE
                key = :key)EOS", tail);
        };

        sqlite3pp::query st_rt_sel_del(*db_, st_rt_sel_text(R"EOS(
                AND a.deleted <> 0
            ORDER BY
                  e.level DESC, e.is_dir, a.entry_id, a.block_no
        )EOS"));

        st_rt_sel_del.bind("key", client_public_key_, sqlite3pp::nocopy);

        sqlite3pp::query st_rt_sel(*db_, st_rt_sel_text(R"EOS(
                AND a.deleted = 0
            ORDER BY
                e.level, e.is_dir DESC, a.entry_id, a.block_no
        )EOS"));

        st_rt_sel.bind("key", client_public_key_, sqlite3pp::nocopy);

        sqlite3pp::command st_rt_del(*db_, R"EOS(
            DELETE FROM remote_tracking WHERE
                entry_id = :entry_id
                AND block_no = :block_no
                AND key = :key
        )EOS");

        st_rt_del.bind("key", client_public_key_, sqlite3pp::nocopy);

        sqlite3pp::transaction tx(*db_, 0);
        auto tx_start = clock_gettime_ns(), tx_now = tx_start;
        decltype(tx_start) tx_delay = std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::milliseconds(50)).count();
        auto tx_ellapsed = [&] { return (tx_now = clock_gettime_ns()) - tx_start; };
        auto tx_deadline = [&] {
            if( tx_ellapsed() >= tx_delay ) {
                tx.commit();
                tx.start();
                tx_start = tx_now;
            }
        };

        auto p_st = &st_rt_sel_del;

        at_scope_exit( drop_entries_cache() );
        init_entries_cache();

        uint64_t tx_bytes, current_entry_id, entry_id;
        auto run = [&] {
            tx_bytes = current_entry_id = 0;

            auto i = p_st->begin();

            if( !i && p_st == &st_rt_sel_del ) {
                p_st = &st_rt_sel;
                i = p_st->begin();
            }

            if( i )
                entry_id = i->get<uint64_t>("entry_id");

            return i;
        };

        tx.start();

        auto e = run();

        ss << (e ? OperationCodeACK : OperationCodeRST);

        while( e ) {
            if( current_entry_id != entry_id ) {
                auto & sser = entry_cache_emplace(entry_id, e);

                ss << sser;

                tx_bytes += sser.raw_bytes();

                current_entry_id = entry_id;
            }

            // send changed blocks
            block_response ssbr;

            ssbr.block_no = e->get<int64_t>("block_no");
            ssbr.deleted  = e->get<uint8_t>("deleted");

            if( ssbr.block_no > 0 )
                ssbr.digest = e->get<std::key512>("block_digest");

            e++;

            entry_id = e->get<uint64_t>("entry_id");

            ssbr.commit = tx_bytes >= TX_MAX_BYTES || (tx_bytes > 0 && tx_ellapsed() >= tx_delay) ? 1 : 0;
            ssbr.next   = e ? current_entry_id != entry_id ? 1 : 0 : 0;
            ssbr.eot    = e ? 0 : 1;

            ss << ssbr;

            tx_bytes += ssbr.raw_bytes();

            st_rt_del.bind("entry_id", current_entry_id);
            st_rt_del.bind("block_no", ssbr.block_no);
            st_rt_del.execute();

            if( ssbr.commit || ssbr.eot ) {
                ss << std::flush;
                // wait for ACK
                uint8_t code;
                ss >> code;

                if( code == OperationCodeRQB ) {
                    blocks_commit rqb;

                    ss >> rqb;

                    for( const auto & pair : rqb.entries ) {
                        auto & sser = entry_cache_file_handle(pair.first, O_RDONLY | O_BINARY);
                        auto buf = std::make_unique<uint8_t[]>(sser.block_size);

                        for( const auto & bno : pair.second ) {
                            auto offset = (bno - 1) * sser.block_size;
                            auto offend = offset + sser.block_size;
                            auto sizerd = offend > sser.file_size ? sser.file_size - offset : sser.block_size;
                            // read file data block
                            auto r = pread(sser.handle, offset, buf.get(), sizerd);
                            if( r == -1 )
                                r = 0;
                            // send data
                            memset(buf.get(), 0, sizerd - r);
                            ss.write(buf.get(), sizerd);
                        }
                    }

                    ss << std::flush;
                    ss >> code;
                }

                if( code != OperationCodeACK )
                    throw std::xsystem_error(EPIPE, std::generic_category(), "Protocol error", __FILE__, __LINE__);

                tx.commit();
                drop_entries_cache();
                tx.start();
                tx_deadline();
                e = run();
            }
        }

        tx.commit();
    };

    while( !shutdown_ ) {
        OperationCode code;
        ss >> code;

        if( code == OperationCodeRQC ) {
            ss >> key_;

            std::unique_lock<std::mutex> lock(trackers_mtx_);
            auto i = std::find_if(trackers_.begin(), trackers_.end(), [&] (const auto & t) {
                return t->key() == key_;
            });

            if( i == trackers_.end() ) {
                lock.unlock();
                ss << OperationCodeBAD << std::flush;
            }
            else {
                dir_user_defined_name_ = (*i)->dir_user_defined_name();
                dir_path_name_ = (*i)->dir_path_name();
                lock.unlock();
                make_path_name();
                send_changes();
            }
        }
    }
}
//------------------------------------------------------------------------------
void remote_directory_tracker::worker()
{
    at_scope_exit( detach_db() );

    std::blob zkey;

    for(;;) {
        try {
            connect_db();

            sqlite3pp::transaction tx(*db_, 0);
            auto tx_start = clock_gettime_ns(), tx_now = tx_start;
            decltype(tx_start) tx_delay = std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::milliseconds(50)).count();
            auto tx_ellapsed = [&] { return (tx_now = clock_gettime_ns()) - tx_start; };
            auto tx_deadline = [&] {
                if( tx_ellapsed() >= tx_delay ) {
                    tx.commit();
                    tx.start();
                    tx_start = tx_now;
                }
            };

            sqlite3pp::command st_ins;
            auto new_st_ins = [&] {
                st_ins = sqlite3pp::command(*db_, R"EOS(
                    INSERT INTO entries (
                        id, is_alive, parent_id, name, level, is_dir, mtime, file_size, block_size, digest
                    ) VALUES (:id, 1, :parent_id, :name, :level, :is_dir, :mtime, :file_size, :block_size, :digest)
                )EOS");
                db_->exceptions(true);
                return st_ins.rc();
            };

            db_->exceptions(false);

            if( new_st_ins() == SQLITE_ERROR ) {
                directory_indexer::create_schema(*db_);
                new_st_ins();
            }

            sqlite3pp::command st_upd(*db_, R"EOS(
                UPDATE entries SET
                    is_alive = 1,
                    parent_id = :parent_id,
                    name = :name,
                    level = :level,
                    is_dir = :is_dir,
                    file_size = :file_size,
                    block_size = :block_size,
                    digest = :digest
                WHERE
                    id = :id
            )EOS");

            sqlite3pp::command st_del(*db_, R"EOS(
                DELETE FROM entries
                WHERE
                    id = :entry_id
            )EOS");

            sqlite3pp::query st_blk_sel(*db_, R"EOS(
                SELECT
                    digest
                FROM
                    blocks_digests
                WHERE
                    entry_id = :entry_id
                    AND block_no = :block_no
            )EOS");

            sqlite3pp::command st_blk_ins(*db_, R"EOS(
                INSERT INTO blocks_digests (
                    entry_id, block_no, mtime, digest
                ) VALUES (
                    :entry_id, :block_no, :mtime, :digest)
            )EOS");

            sqlite3pp::command st_blk_upd(*db_, R"EOS(
                UPDATE blocks_digests SET
                    digest = :digest,
                    mtime = :mtime
                WHERE
                    entry_id = :entry_id
                    AND block_no = :block_no
            )EOS");

            sqlite3pp::command st_blk_del(*db_, R"EOS(
                DELETE FROM blocks_digests
                WHERE
                    entry_id = :entry_id
                    AND block_no = :block_no
            )EOS");

            channel_->enqueue([&] (socket_stream & ss) {
                // request server side changes
                ss << ServerModuleRDT << OperationCodeRQC << key_ << std::flush;

                OperationCode ret_code;
                ss >> ret_code;

                if( ret_code != OperationCodeACK ) // no changes
                    return;

                tx.start();

                at_scope_exit( drop_entries_cache() );
                init_entries_cache();

                block_response ssbr;
                blocks_commit rqb;

                auto get_entry = [&] {
                    entry_response sser;
                    ss >> sser;
                    return entry_cache_emplace(sser.entry_id, sser);
                };

                do {
                    auto sser = get_entry();

                    entry_cache_file_handle(sser.entry_id, O_CREAT | O_RDWR | O_BINARY);

                    if( !sser.is_dir && ftruncate(sser.handle, sser.file_size) == -1 ) {
                        auto err = errno;
                        std::cerr
                            << std::xsystem_error(
                                err, std::system_category(), "ftruncate error: " + entry_cache_path_name(sser.entry_id), __FILE__, __LINE__)
                            << std::endl;
                    }

                    do {
                        // receive and write blocks changed on server side if it's not equal on client side
                        ss >> ssbr;
                        rqb.emplace_back(sser.entry_id, ssbr.block_no);

                        auto bind = [&] (auto & st) {
                            st.bind("id", sser.entry_id);
                            st.bind("parent_id", sser.parent_id);
                            st.bind("name", sser.name, sqlite3pp::nocopy);
                            st.bind("level", sser.level);
                            st.bind("is_dir", sser.is_dir ? 1 : 0);
                            st.bind("file_size", sser.file_size);
                            st.bind("block_size", sser.block_size);

                            if( sser.is_dir )
                                st.bind("digest", zkey, sqlite3pp::nocopy);
                            else
                                st.bind("digest", sser.digest, sqlite3pp::nocopy);
                        };

                        bind(st_ins);

                        db_->exceptions(false);
                        st_ins.execute();
                        db_->exceptions(true);

                        if( st_ins.rc() == SQLITE_CONSTRAINT_UNIQUE ) {
                            bind(st_upd);
                            st_upd.execute();
                        }

                        if( ssbr.block_no == 0 ) {
                            // mtime or digest or new entry
                        }
                        else if( ssbr.block_no == int64_t(-1) ) {
                            // delete entry
                            auto path_name = entry_cache_path_name(sser.entry_id);

                            if( sser.is_dir )
                                rmdir(path_name);
                            else
                                unlink(path_name);

                            st_del.bind("entry_id", sser.entry_id);
                            st_del.execute();
                        }
                        else if( ssbr.deleted ) {
                            // delete block
                            st_blk_del.bind("entry_id", sser.entry_id);
                            st_blk_del.bind("block_no", ssbr.block_no);
                            st_blk_del.execute();
                        }
                        else {
                            at_scope_exit( st_blk_sel.reset() );

                            st_blk_sel.bind("entry_id", sser.entry_id);
                            st_blk_sel.bind("block_no", ssbr.block_no);

                            auto i = st_blk_sel.begin();

                            if( !i || i->get<std::key512>("digest") != sser.digest )
                                rqb.emplace_back(sser.entry_id, ssbr.block_no);
                        }

                        tx_deadline();

                        if( ssbr.commit ) {
                            if( !rqb.entries.empty() ) {
                                ss << OperationCodeRQB << rqb << std::flush;

                                for( const auto & pair : rqb.entries ) {
                                    auto & e = entry_cache_file_handle(pair.first, O_CREAT | O_RDWR | O_BINARY);
                                    auto buf = std::make_unique<uint8_t[]>(sser.block_size);

                                    for( const auto & bno : pair.second ) {
                                        auto offset = (bno - 1) * e.block_size;
                                        auto offend = offset + e.block_size;
                                        auto sizerd = offend > e.file_size ? e.file_size - offset : e.block_size;
                                        // recv block data
                                        ss.read(buf.get(), sizerd);
                                        // write data to file
                                        auto r = pwrite(e.handle, offset, buf.get(), sizerd);

                                        if( r == -1 ) {
                                            auto err = errno;
                                            std::cerr
                                                << std::xsystem_error(
                                                    err, std::system_category(), "file block data write error", __FILE__, __LINE__)
                                                << std::endl;
                                        }
                                        else if( decltype(sizerd) (r) < sizerd ) {
                                            std::cerr
                                                << std::xsystem_error(
                                                    ENOSPC, std::system_category(), "disk device was filled", __FILE__, __LINE__)
                                                << std::endl;
                                        }

                                        st_blk_ins.bind("entry_id", e.entry_id);
                                        st_blk_ins.bind("block_no", bno);

                                        db_->exceptions(false);
                                        st_blk_ins.execute();
                                        db_->exceptions(true);

                                        if( st_blk_ins.rc() == SQLITE_CONSTRAINT_UNIQUE ) {
                                            st_blk_upd.bind("entry_id", e.entry_id);
                                            st_blk_upd.bind("block_no", bno);
                                            st_blk_upd.execute();
                                        }

                                        tx_deadline();
                                    }
                                }

                                rqb.entries.clear();
                            }

                            tx.commit();

                            drop_entries_cache();

                            tx.start();

                            if( !ssbr.eot )
                                ss << OperationCodeACK << std::flush;
                        }

                    } while( !ssbr.next );
                } while( !ssbr.eot );

                tx.commit();
            });
        }
        catch( const std::exception & e ) {
            std::cerr << e << std::endl;
            detach_db();
        }

        std::unique_lock<std::mutex> lk(mtx_);

        if( cv_.wait_for(lk, std::chrono::seconds(60), [&] { return shutdown_ || oneshot_; }) )
            break;
    }
}
//------------------------------------------------------------------------------
void remote_directory_tracker::startup()
{
    if( started_ )
        return;

    channel_ = std::make_unique<client>();
    channel_->server_public_key(server_public_key_);
    channel_->client_public_key(client_public_key_);
    channel_->startup();

    db_suffix_ = "-REMOTE";
    make_path_name();

    shutdown_ = false;
    worker_result_ = thread_pool_t::instance()->enqueue(&remote_directory_tracker::worker, this);
    started_ = true;
}
//------------------------------------------------------------------------------
void remote_directory_tracker::shutdown()
{
    if( !started_ )
        return;

    channel_->shutdown();

    std::unique_lock<std::mutex> lk(mtx_);
    shutdown_ = true;
    lk.unlock();
    cv_.notify_one();

    worker_result_.wait();
    channel_ = nullptr;
    started_ = false;
}
//------------------------------------------------------------------------------
} // namespace homeostas
//------------------------------------------------------------------------------
